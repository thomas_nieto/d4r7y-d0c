# -*- mode: org; -*-

* Réunion [2019-03-07 Thu] - évolution WS SIEBEL
  Sujets :
    - *LONGLIFE*
    - *CAGNOTTE*
    - *DARTY PREMIUM*.

  Impacts dans SIEBEL -> nouveaux tags pour le moteur de promo.

  On parle de la date de sortir pour *CAGNOTTE*.

** *DARTY PREMIUM*
   Équivalent de client Fnac, *FNAC ONE*. Ce sont des avantages pour les clients fidèles et mis en priorité sur le SAV notamment, créneaux de livraison. (Production en juin souhaité). Rejoint pas mal le scope de fonctionnalité de *BOUTON*.

   Métier en attente d'un macro-chiffrage pour mi-mars.

   Lien entre tag VIP et les promos...

** maj du service de reco client SIEBEL
   Pour intégrer les nouveaux types de clients comme les clients *LONG LIFE*, *DARTY PREMIUM*, etc.

*** *LONG LIFE*
    Reco client prévu pour fin avril pour permettre le développement côté back. Contrats d'interface communiqués pour mi-mars.

** quid du client qui a plusieurs avantages
   Ex. : quels promos s'appliquent si le client est *D+* et *DARTY PREMIUM* et *LONG LIFE* ? Situation pas claire.

   Le métier n'a pas statué sur la question. Propositions à faire au métier.

** *CAGNOTTE*
   sortie prévue pour octobre (juste en magasin).

** questions
*** équipe innovante, c'est qui, c'est quoi
    application vente côté magasin. Que sur le stock magagsin (p-e entrepot).

    Back ne gere que le back DCOM.

*** qui étaient les participants
    Olivier Mallet : MOA moteur de promo pricing

    Thomas Morreau : Celia côté promo et pricing

*** SIEBEL service de reco-client
*** tags sur le moteur de promo
    Sophie //- à la discussion sur la maj du service reco client SIEBEL

    ce sont les flags de SIEBEL qui permettent de marquer les clients par rapport à leur avantages.

*** projet *CAGNOTTE*
    Points fidelités (vrais euros).

    Cumulé sur le compte fidélité.

    obj. pouvoir payer une commande avec ces points.

*** niveau de couverture LONGLIFE
    plusieurs types de couverture par rapport au produit. Voir [[file:20181106_Pr%C3%A9sentation_Longlife.pdf][pdf de presentation]].

** remarques
   On voit se dégager des typologies de clients. Cette notion n'apparît pas dans le back.

   Le "métier" n'est pas présent à la réu. Chelou... En fait, j'ai l'impression que ce qui se nomme le métier ici c'est, en fait, la hierarchie.

   Ne connaissent pas leur applis
